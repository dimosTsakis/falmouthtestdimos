﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTestDimos.Models
{
    public class LocalImage
    {
        public int LocalImageId { get; set; }
        public HttpPostedFileWrapper LocalImageFile { get; set; }
    }
}