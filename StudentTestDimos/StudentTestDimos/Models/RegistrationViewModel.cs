﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentTestDimos.Models
{
    public class RegistrationViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
    }
}