﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentTestDimos.Models;

namespace StudentTestDimos.Controllers
{
    public class AccountManagementController : Controller
    {
        private PatientZeroEntities db = new PatientZeroEntities();

        // GET: AccountManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegisterUser(RegistrationViewModel model)
        {
            string messageToReturn = "";

            //Check if user with the same name already exists.
            SiteUser user = db.SiteUsers.SingleOrDefault(x => x.Username == model.Username);

            if(user != null)
            {
                messageToReturn = "User already exists!";
            }
            else
            {
                int nextId = db.SiteUsers.Count();

                SiteUser newUser = new SiteUser();
                newUser.Id = nextId;
                newUser.Username = model.Username;
                newUser.Password = model.Password;

                //RoleId = 1 is for common users. You CAN'T register as admin (admin RoleId is 0).
                //TODO if there is time: separate table to look up user roles.
                newUser.RoleId = 1;

                db.SiteUsers.Add(newUser);
                messageToReturn = "Success!";
                db.SaveChanges();
            }

            return Json(messageToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoginUser(RegistrationViewModel model)
        {
            string messageToReturn = "Failed";

            //Check if user exists in database.
            SiteUser user = db.SiteUsers.SingleOrDefault(x => x.Username == model.Username &&
                                                              x.Password == model.Password);

            if(user != null)
            {
                Session["UserId"] = user.Id;
                Session["RoleId"] = user.RoleId;
                Session["UserName"] = user.Username;

                //Check if the user is admin or not.
                if(user.RoleId == 1)
                {
                    messageToReturn = "NormalUser";
                }
                else if(user.RoleId == 0)
                {
                    messageToReturn = "Admin";
                }
            }

            return Json(messageToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}