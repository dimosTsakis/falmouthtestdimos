﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentTestDimos.Models;
using Google.Cloud.Vision.V1;
using System.Drawing;

namespace StudentTestDimos.Controllers
{
    public class PhotosController : Controller
    {

        private PatientZeroEntities db = new PatientZeroEntities();

        // GET: Photos
        public ActionResult Index()
        {
            ViewBag.myList = new SelectList(db.Photos.ToList(), "Id", "ImagePath");
            return View(db.Photos.ToList());
        }

        public JsonResult GetImagePaths()
        {
            List<Photo> myList = db.Photos.ToList();
            List<string> result = new List<string>();

            for(int photoIndex = 0; photoIndex < myList.Count; photoIndex++)
            {
                result.Add(myList[photoIndex].ImagePath + "*" + myList[photoIndex].ImagePathFace);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ImageUpload(LocalImage model)
        {
            var file = model.LocalImageFile;

            if(file != null)
            {
                //Save the full body image to the folder.
                file.SaveAs(Server.MapPath("/Content/Photos/" + file.FileName));

                //--------------------------------Configure google api.--------------------------------
                //NOTE: Place the json file in the Content folder and paste its name here.
                //Take a look at "Set up a service account" here https://cloud.google.com/vision/docs/common/auth#authenticating_with_application_default_credentials for more information.

                string jsonFileName = "FalmouthRecruitmentTestDimos-0cb86735a531.json";
                string projectPath = System.AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/");
                System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", projectPath + "Content/" + jsonFileName);

                //Get image of the head.
                var client = ImageAnnotatorClient.Create();
                var stringPath = projectPath + "Content/Photos/" + file.FileName;

                //Currenty it supports only pictures with 1 face! (if you provide a picture with more than 1 faces, it will just take the first).
                var response = client.DetectFaces(Google.Cloud.Vision.V1.Image.FromFile(stringPath));
                //Get the original full body image.
                var image = System.Drawing.Image.FromFile(stringPath);
                //Scales the final head image (if needed).
                int scaleFactor = 1;

                /*  There are 4 vertices that make the bounding box for the face.
                    BoundingPoly.Vertices[0] is Top Left
                    BoundingPoly.Vertices[1] is Top Right
                    BoundingPoly.Vertices[2] is Bottom Right
                    BoundingPoly.Vertices[3] is Bottom Left 
                */

                Rectangle sourceRec = new Rectangle(response[0].BoundingPoly.Vertices[0].X,
                                                    response[0].BoundingPoly.Vertices[0].Y,
                                                    response[0].BoundingPoly.Vertices[1].X - response[0].BoundingPoly.Vertices[0].X,
                                                    response[0].BoundingPoly.Vertices[3].Y - response[0].BoundingPoly.Vertices[0].Y);

                Rectangle destRec = new Rectangle(0,
                                                    0,
                                                    (response[0].BoundingPoly.Vertices[1].X - response[0].BoundingPoly.Vertices[0].X) * scaleFactor,
                                                    (response[0].BoundingPoly.Vertices[3].Y - response[0].BoundingPoly.Vertices[0].Y) * scaleFactor);

                //That's the face bitmap.
                Bitmap nb = new Bitmap((response[0].BoundingPoly.Vertices[1].X - response[0].BoundingPoly.Vertices[0].X) * scaleFactor,
                                        (response[0].BoundingPoly.Vertices[3].Y - response[0].BoundingPoly.Vertices[0].Y) * scaleFactor);

                Graphics e = Graphics.FromImage(nb);
                e.DrawImage(image, destRec, sourceRec, GraphicsUnit.Pixel);

                //Create the path.
                int lastDot = stringPath.LastIndexOf('.');
                string outFilePath = lastDot < 0 ?
                    stringPath + ".face" :
                    stringPath.Substring(0, lastDot) + ".face" + stringPath.Substring(lastDot);

                //Save the face image to the folder.
                nb.Save(outFilePath);

                //Save the two image paths to the database.
                try
                {
                    int nextId = db.Photos.Count();

                    Photo newPhoto = new Photo();
                    newPhoto.Id = nextId;
                    newPhoto.ImagePath = "~/Content/Photos/" + file.FileName;

                    string relativeFileNameForFace = "~" + outFilePath.Substring(outFilePath.IndexOf("/Content"));
                    newPhoto.ImagePathFace = relativeFileNameForFace;

                    db.Photos.Add(newPhoto);
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    //TODO: Logging if there is time.
                    throw ex;
                }
            }

            return Json(file.FileName, JsonRequestBehavior.AllowGet);
        }
    }
}